---
This is a flutter github caching project

---

## Requirement
* This project is build with flutter **2.10.5**.

## Tested Operating Systems
* For android I tested from android **8 to 12**.
* For ios I tested in **iPhone 13** which os version is **15.4**.

## Additional libraries
* Dio is for **API** fetching.
* Flutter bloc is used for **business logic** and **design pattern**.
* Get it is used for **dependency injection**.
* Equatable is used for **object comparing**.
* Shared pref is used for saving **cache** and save **app state**.
* Cached network image which has ability to show image in **offline** since once it fetched an image from internet then it can **cache** that image to show in **offline**.
* Connectivity plus is used to detect that device is **connected** to the **internet** or not.
* Intl is used for **custom date time format**.
* Flutter flavor is used to apply different **flavor**.

# Download link
Application apk can be download from the following link:

<a href="https://drive.google.com/file/d/1zdJAP-o-vcneMje2z3e0AXHAEvqdSTl5/view?usp=sharing">DOWNLOAD APK FROM HERE!</a>

## List and details screen
<p float="left">
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/flavour_cache.png" width="360" height="760" />
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/details_screen.png" width="360" height="760" />
</p>

## Two flavors are used (Cached and No Cache)
<p float="left">
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/flavour_cache.png" width="360" height="760" />
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/flavour_no_cache.png" width="360" height="760" />
</p>

Two types of flavor are used in this app, one is **cached** and another is **no cache**. Cached flavor is using caching mechanism on the other hand no cache flavor use direct response only.

## Flavor running technique
<p float="left">
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/flavour_run.png"/>
</p>

**Two** types of flavor can be run by using different **main** dart file.

## Unit and Widget testing

**Unit** and **Widget** testing is implemented in test folder with documentation.

## Error Dialog
<p float="left">
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/error.png" width="360" height="760"/>
</p>

An **error** message will show when github api limit reached for **same IP** address. It occurs because here I am not using any **github login token**. Login system with **token** can resolve this issue. So need to press **retry** button at least **10-15** seconds later. All error messages are **centrally customized**.

## Flow diagram
<p float="left">
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/query_diagram.png"/>
  <img src="https://gitlab.com/shimul6680/sovware_github_test_project/-/raw/master/scrshot/flow_diagram.png"/>
</p>

Three types of **query** are used here which are **no filter, sort and updated**. Each individual types of query has **page number**. For each types of **query** with **page number** works according to **flow diagram**.




