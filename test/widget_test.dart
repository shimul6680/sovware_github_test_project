import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/data_for_details_screen.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/data_for_list_screen.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/screen_data.dart';
import 'package:sovware_github_test_project/screen/details_screen/widgets/details_screen_ui.dart';
import 'package:sovware_github_test_project/screen/list_screen/inner_widgets/inner_widget/list_body_widget.dart';
import 'package:sovware_github_test_project/screen/list_screen/inner_widgets/radio_group/inner_widget/radio_button_widget.dart';

void main() {
  //test that radio button widget has no error in a row
  testWidgets('Test button widget', (tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: Row(
          children: [
            RadioButtonWidget(
              title: 'No Filter',
              value: 1,
              groupValue: 1,
              onChanged: (value) {},
            ),
          ],
        ),
      ),
    ));

    final textWidget = find.byType(Text);

    expect(textWidget, findsOneWidget);
  });

  //test that widget data bundle data is showing properly

  testWidgets('Test List Body test', (tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: ListBodyWidget(
          curScreenDataPaginationList: const [
            ScreenData(
                DataForListScreen(
                  projectName: 'Flutter Widget Testing',
                  language: 'English',
                  license: '313',
                  photoUrl: 'https://photos.com/test.png',
                  stars: 208,
                  dateTime: null,
                ),
                DataForDetailsScreen(
                  ownersName: 'Shimul',
                  photoUrl: 'https://photos.com/test.png',
                  repDescription: 'Description Test',
                  lastUpdateDateTime: null,
                )
            )
          ],
          reachAtBottom: () {},
        ),
      ),
    ));

    final listViewWidget = find.byType(ListView);

    expect(listViewWidget, findsOneWidget);
    expect(find.text('Flutter Widget Testing'), findsOneWidget);
    expect(find.text('Language: English'), findsOneWidget);
    expect(find.text('License: 313'), findsOneWidget);
    expect(find.text('☆: 208'), findsOneWidget);
    expect(find.text('Date Time: null'), findsOneWidget);

    final cachedNetworkImage = find.byType(CachedNetworkImage).evaluate().single.widget as CachedNetworkImage;

    expect('https://photos.com/test.png', cachedNetworkImage.imageUrl);
  });

  //test that widget data bundle data is showing properly

  testWidgets('Details Body Test', (tester) async {
    await tester.pumpWidget(
      const MaterialApp(
        home: Scaffold(
          body: DetailsScreen(
            index: 0,
            dataForDetailsScreen: DataForDetailsScreen(
              ownersName: 'Shimul',
              photoUrl: 'https://photos.com/test.png',
              repDescription: 'Description Test',
              lastUpdateDateTime: '30-10-2022 12:20 pm',
            ),
          ),
        ),
      ),
    );

    final detailsWidget = find.byType(DetailsScreen);

    expect(detailsWidget, findsOneWidget);
    expect(find.text('Owner Name: Shimul'), findsOneWidget);
    expect(find.text('Date Time: 30-10-2022 12:20 pm'), findsOneWidget);
    expect(find.text('Description: Description Test'), findsOneWidget);
  });
}
