import 'package:flutter_test/flutter_test.dart';
import 'package:sovware_github_test_project/api/model/inner_model/item.dart';
import 'package:sovware_github_test_project/api/model/inner_model/license.dart';
import 'package:sovware_github_test_project/api/model/inner_model/owner.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/data_for_details_screen.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/data_for_list_screen.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/inner_model/screen_data.dart';
import 'package:sovware_github_test_project/screen/app_screen_data_model/screen_data_bundle_model.dart';

void main(){
  //show that data bundle unit which create from api model is correct
  test('Screen bundle unit test', () async{
    //generate value from direct constructor
    const expectedValue = ScreenDataBundle(true, false,[
      ScreenData(
        DataForListScreen(
          projectName: 'Flutter Unit Testing',
          language: 'English',
          license: '313',
          photoUrl: 'https://photos.com/test.png',
          stars: 208,
          dateTime: null,
        ),
        DataForDetailsScreen(
          ownersName: 'Shimul',
          photoUrl: 'https://photos.com/test.png',
          repDescription: 'Description Test',
          lastUpdateDateTime: null
        )
      )
    ]);

    final testRepository = TestRepository();

    //generate value from mock api model class
    final mockResponse = await testRepository.getListDataBundle(
      fromCache: true,
      deviceIsConnected: false,
      items: [
        Item(
          name: 'Flutter Unit Testing',
          language: 'English',
          license: License(spdxId: '313'),
          pushedAt: null,
          stargazersCount: 208,
          owner: Owner(
            avatarUrl: 'https://photos.com/test.png',
            login: 'Shimul',
          ),
          description: 'Description Test',
          updatedAt: null,
        )
      ]
    );
    expect(expectedValue, mockResponse);
  });
}

class TestRepository{
  Future<ScreenDataBundle> getListDataBundle({
    required bool fromCache,
    required bool deviceIsConnected,
    required List<Item> items,
  }){
    return Future.value(ScreenDataBundle.fromItemList(
      fromCache: fromCache,
      deviceIsConnected: deviceIsConnected,
      items: items,
    ));
  }
}