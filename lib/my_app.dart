import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'app_router/app_router.dart';
import 'injection_container/injection_container.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key, this.themeColor}) : super(key: key);

  final MaterialColor? themeColor;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FlavorBanner(
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: themeColor),
        onGenerateRoute: getIt<AppRouter>().onGenerateRoute,
        //home: const TestScreen(),
      ),
    );
  }
}

class TestScreen extends StatelessWidget {
  const TestScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('Hello'),
            TextField()
          ],
        ),
      ),
    );
  }
}
