import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:sovware_github_test_project/app_constants/app_constants.dart';
import 'package:sovware_github_test_project/pre_load_work/pre_load_work.dart';
import 'my_app.dart';

//brain station gitlab transfer test again
//push test
void main() async{
  await preLoadWork();
  FlavorConfig(
    name: kNoCache,
    color: Colors.red,
    location: BannerLocation.bottomEnd,
    variables: {
      "cacheDurationInMinute": 0,
    },
  );
  runApp(const MyApp(themeColor: Colors.red));
}
