import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:sovware_github_test_project/pre_load_work/pre_load_work.dart';
import 'app_constants/app_constants.dart';
import 'my_app.dart';

//brain station gitlab transfer test again
//pipeline disable trying!
void main() async{
  await preLoadWork();
  FlavorConfig(
    name: kCached,
    color: Colors.blue,
    location: BannerLocation.bottomEnd,
    variables: {
      "cacheDurationInMinute": kAppCacheTimeDuration,
    },
  );
  runApp(const MyApp(themeColor: Colors.blue));
}

