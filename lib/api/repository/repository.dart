import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:sovware_github_test_project/api/cache_use_case/cache_use_case.dart';
import 'package:sovware_github_test_project/api/model/github_list_response.dart';
import 'package:sovware_github_test_project/app_connectivity/connectivity_use_case.dart';
import 'package:sovware_github_test_project/app_constants/app_constants.dart';
import '../../screen/app_screen_data_model/screen_data_bundle_model.dart';

abstract class GitHubListRepository{
    Future<ScreenDataBundle> getListDataBundle({required Map<String, dynamic> queryParameters});
}

class GitHubListRepositoryImp implements GitHubListRepository{
  final Dio dio;
  final CacheUseCase cacheUseCase;
  final ConnectivityUseCase connectivityUseCase;
  late bool deviceIsConnected;

  GitHubListRepositoryImp({
    required this.dio,
    required this.connectivityUseCase,
    required this.cacheUseCase,
  });

  bool get _flavourIsCached => FlavorConfig.instance.name == kCached;

  @override
  Future<ScreenDataBundle> getListDataBundle({required Map<String, dynamic> queryParameters}) async{
    late final String? response;
    late final bool fromCache;
    deviceIsConnected = await connectivityUseCase.deviceIsConnected;

    if(_flavourIsCached && cacheUseCase.isFromCache(queryParameters: queryParameters, isConnected: deviceIsConnected)){
      response = cacheUseCase.getResponseFromCache(queryParameters: queryParameters);
      fromCache = true;
    }else{
      try{
        final curRes = await dio.get(searchRepositoryEndPoint, queryParameters: queryParameters);
        final statusCode = curRes.statusCode;
        response = curRes.data;
        if(curRes.statusCode != 200){
          final curJsonMap = jsonDecode(response!);
          final centralMessage = curJsonMap['central_error'];
          final apiMessage = curJsonMap['message'];
          return Future.error('$centralMessage\n$apiMessage, Response code: $statusCode');
        }
        else{
          if(_flavourIsCached){
            await cacheUseCase.saveResponseToCache(queryParameters: queryParameters, response: response!);
          }
        }
      }
      catch(e){
        return Future.error(e);
      }
      fromCache = false;
    }

    print('$queryParameters From Cache: ${fromCache ? 'Yes' : 'No'}');

    //implement isolate
    final details = await compute(_parseList, response!);
    return ScreenDataBundle.fromItemList(fromCache: fromCache, items: details.items, deviceIsConnected: deviceIsConnected);
  }

  //isolate use here for faster parsing
  static GithubListResponse _parseList(String jsonResponse) {
    final jsonObj = json.decode(jsonResponse);
    return GithubListResponse.fromMap(jsonObj);
  }

}