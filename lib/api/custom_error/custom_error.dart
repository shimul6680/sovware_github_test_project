import 'package:dio/dio.dart';

class CustomDioError extends DioError{
  CustomDioError({required RequestOptions requestOptions, required this.customMessage}) : super(requestOptions: requestOptions);

  final String customMessage;

  @override
  String toString(){
    return customMessage;
  }

}