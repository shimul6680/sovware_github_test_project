import 'package:flutter/cupertino.dart';
import '../injection_container/injection_container.dart';

Future<void> preLoadWork() async{
  WidgetsFlutterBinding.ensureInitialized();
  await registerAllDependency();
}